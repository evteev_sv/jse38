package com.nlmk.evteev.exception;

public class ChuckException extends RuntimeException {

    public ChuckException(java.lang.String message) {
        super(message);
    }
}
