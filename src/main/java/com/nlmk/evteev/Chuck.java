package com.nlmk.evteev;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmk.evteev.exception.ChuckException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

public class Chuck {

    private static final String CHUCK_URI = "https://api.chucknorris.io/jokes/random?category=";
    private static final String CATEGORY_URI = "https://api.chucknorris.io/jokes/categories";
    private static final String CHUCK_PREFIX = "Joke about ";
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;


    public Chuck(HttpClient httpClient, ObjectMapper objectMapper) {
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }

    private List<String> getCategoriesJoke() throws IOException, InterruptedException {
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create(CATEGORY_URI)).GET().build();
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        JsonNode jsonNode = objectMapper.readTree(httpResponse.body());
        return objectMapper.convertValue(jsonNode, List.class);
    }

    public void getRandomJoke() {
        try {
            getCategoriesJoke().parallelStream()
                    .map(category -> HttpRequest.newBuilder().uri(URI.create(CHUCK_URI + category)).GET().build())
                    .map(this::getJokeResponse)
                    .map(this::getBody)
                    .map(jsonNode -> CHUCK_PREFIX + jsonNode.get("categories").get(0).asText() + ": " + jsonNode.get("value").asText())
                    .forEach(System.out::println);
        } catch (InterruptedException | IOException exception) {
            Thread.currentThread().interrupt();
        }
    }

    private HttpResponse<String> getJokeResponse(HttpRequest httpRequest) throws ChuckException {
        try {
            return httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new ChuckException(e.getMessage());
        }
    }

    private JsonNode getBody(HttpResponse<String> httpResponse) throws ChuckException {
        try {
            return objectMapper.readTree(httpResponse.body());
        } catch (JsonProcessingException e) {
            throw new ChuckException(e.getMessage());
        }
    }

}
