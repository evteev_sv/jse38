package com.nlmk.evteev;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.http.HttpClient;

public class Application {

    public static void main(String[] args) {
        Chuck chuck = new Chuck(HttpClient.newBuilder().build(), new ObjectMapper());
        chuck.getRandomJoke();
    }
}
